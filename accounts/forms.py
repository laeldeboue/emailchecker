from django import forms
from django.contrib.auth import authenticate, get_user_model
from django.utils.translation import gettext_lazy as _


User = get_user_model()


class UserRegisterForm(forms.ModelForm):
    password = forms.CharField(label=_("Mot de passe"))
    
    class Meta:
        model = User
        fields = ("email", 'password')
        
    def clean(self, *args, **kwargs):
        email = self.cleaned_data.get("email")
        password = self.cleaned_data.get("password")
        email_check = User.objects.filter(email=email)
        if email_check.exists():
            raise forms.ValidationError(_("Cette adresse email existe déjà !"))
        if len(password) < 6:
            raise forms.ValidationError(_("Mot de passe doit comporter au moins 6 caractères !"))
    
        return super(UserRegisterForm, self).clean(*args, **kwargs)
