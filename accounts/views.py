import httpx
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.contrib import messages
from django.core.mail import EmailMessage

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, get_user_model, login
from .forms import UserRegisterForm


User = get_user_model()


def home(request):
    BASE_URL = "https://api.ote-godaddy.com"
    headers = {'X-Shopper-Id': '1234567890'}
    endpoint = f"{BASE_URL}/v1/domains/"
    domains = httpx.get(endpoint, headers=headers)
    template_name = "accounts/home.html"
    context = {
        'name': "BOUE",
        'domains': domains,
    }
    return render(request, template_name, context)

def verify_email(request):
    if request.method == 'POST':
        if request.user.email_is_verified:
            current_site = get_current_site(request)
            user = request.user
            email = request.user.email
            subject = _("Email de vérification")
            message = render_to_string('accounts/verify-email-message.html', {
                'request': request,
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            email = EmailMessage(
                subject, message, to=[email]
            )
            email.content_subtype = 'html'
            email.send()
            return redirect('user:verify-email-done')
        else:
            return redirect('user:signup')
    
    return render(request, 'accounts/verify-email.html')


def verify_email_done(request):
    return render(request, 'accounts/verify-email-done.html')


def verify_email_confirm(request, uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    
    if user is not None and account_activation_token.check_token(user, token):
        user.email_is_verified = True
        user.save()
        messages.success(request, "Votre adresse email a été vérifié avec succès.")
        return redirect('user:verify-email-complete')
    else:
        messages.warning(request, "Le lien est invalide.")
        
    return render(request, "accounts/verify-email-confirm.html")


def verify_email_complete(request):
    return render(request, 'accounts/verify-email-complete.html')        

def signup_view(request):
    template_name = 'accounts/signup.html'
    if request.method == 'POST':
        next = request.GET.get('next')
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            password = form.cleaned_data.get('password')
            user.save()
            new_user = authenticate(email=user.email, password=password)
            login(request, new_user)
            if next:
                return redirect(next)
            else:
                return redirect('verify-email')
            
    else:
        form = UserRegisterForm()
        
    context = {
        'form': form,
    }
    return render(request, template_name, context)